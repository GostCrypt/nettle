/* ecc-eh-to-a-gost.c

   Copyright (C) 2020 Dmitry Baryshkov

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>

#include "ecc.h"
#include "ecc-internal.h"

/* Note: it does not handle op > 1, it should be handled separately in curve code */
void
ecc_eh_to_a_gost (const struct ecc_curve *ecc,
		  const mp_limb_t *ecc_s, const mp_limb_t *ecc_t,
		  int op,
		  mp_limb_t *r, const mp_limb_t *p,
		  mp_limb_t *scratch)
{
  /* a = W - V
     b = a * U
     c = b^-1
     d = W+V
     e = s * d
     f = c * e
     g = f * U
     X = g + t
     h = f * U
     Y = h */
#define a (scratch + 2*ecc->p.size)
#define b (scratch)
#define c (scratch + ecc->p.size)
#define d (scratch + 4*ecc->p.size)
#define e (scratch + 2*ecc->p.size)
#define f (scratch + 3*ecc->p.size)
#define g (scratch)
#define h (scratch)
#define s (scratch + 3*ecc->p.size)

#define U (p)
#define V (p + ecc->p.size)
#define W (p + 2*ecc->p.size)

  ecc_mod_sub (&ecc->p, a, W, V);
  ecc_mod_mul (&ecc->p, b, a, U);
  ecc->p.invert (&ecc->p, c, b, s);
  ecc_mod_add (&ecc->p, d, W, V);
  ecc_mod_mul (&ecc->p, e, ecc_s, d);
  ecc_mod_mul (&ecc->p, f, e, c);
  ecc_mod_mul (&ecc->p, h, f, U);
  ecc_mod_add (&ecc->p, r, h, ecc_t);

  if (op == 0)
    {
      ecc_mod_mul (&ecc->p, h, f, W);
      mpn_copyi (r + ecc->p.size, h, ecc->p.size);
    }

  /* op > 1 is handled in curve code */

#undef a
#undef b
#undef c
#undef d
#undef e
#undef f
#undef g
#undef h
#undef s

#undef U
#undef V
#undef W
}
