/* ecc-gost-gc256a.c

   Compile time constant (but machine dependent) tables.

   Copyright (C) 2016, 2019 Dmitry Eremin-Solenikov

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

/* Development of Nettle's ECC support was funded by the .SE Internet Fund. */

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include <assert.h>

#include "ecc.h"
#include "ecc-internal.h"

#define USE_REDC 0

#include "ecc-gost-gc256a.h"

static void
ecc_gost_gc256a_modp (const struct ecc_modulo *m, mp_limb_t *rp)
{
  mp_size_t mn = m->size;
  mp_limb_t hi;

  hi = mpn_addmul_1(rp, rp + mn, mn, 0x269);
  hi = sec_add_1 (rp, rp, mn, hi * 0x269);
  hi = sec_add_1 (rp, rp, mn, hi * 0x269);
  assert(hi == 0);
}

#define QHIGH_BITS (GMP_NUMB_BITS * ECC_LIMB_SIZE - 254)

#if QHIGH_BITS == 0
#error Unsupported limb size
#endif

static void
ecc_gost_gc256a_modq (const struct ecc_modulo *q, mp_limb_t *rp)
{
  mp_size_t n;
  mp_limb_t cy;

  /* n is the offset where we add in the next term */
  for (n = ECC_LIMB_SIZE; n-- > 0;)
    {
      cy = mpn_submul_1 (rp + n,
			 q->B_shifted, ECC_LIMB_SIZE,
			 rp[n + ECC_LIMB_SIZE]);
      /* Top limb of mBmodq_shifted is zero, so we get cy == 0 or 1 */
      assert (cy < 2);
      cnd_add_n (cy, rp+n, q->m, ECC_LIMB_SIZE);
    }

  cy = mpn_submul_1 (rp, q->m, ECC_LIMB_SIZE,
		     rp[ECC_LIMB_SIZE-1] >> (GMP_NUMB_BITS - QHIGH_BITS));
  assert (cy < 2);
  cnd_add_n (cy, rp, q->m, ECC_LIMB_SIZE);
}

static int
ecc_gost_gc256a_is_valid (const struct ecc_curve *ecc,
		     const mpz_t x, const mpz_t y)
{
  mp_size_t size;
  mp_limb_t *tmp;
  mpz_t u, v, w, d;
  mpz_t u2, v2, w2;
  mpz_t lhs, rhs;
  mpz_t t;
  int res;

  size = ecc->p.size;

  if (mpz_sgn (x) < 0 || mpz_limbs_cmp (x, ecc->p.m, size) >= 0
      || mpz_sgn (y) < 0 || mpz_limbs_cmp (y, ecc->p.m, size) >= 0)
    return 0;

  tmp = gmp_alloc_limbs ((3 + 2) * size + ECC_A_TO_EH_GOST_ITCH(size));

  mpz_limbs_copy (tmp + 3*size, x, size);
  mpz_limbs_copy (tmp + 4*size, y, size);

  ecc->a_to_h (ecc, tmp, tmp + 3*size, tmp + 5*size);

  /* Now we have homogeneous coordinates on birationally equivalent Edwards
   * curve. Use them. */

  /* Use mpz_t. Might be ineffective but simpler for now */

  mpz_roinit_n (u, tmp, size);
  mpz_roinit_n (v, tmp+size, size);
  mpz_roinit_n (w, tmp+2*size, size);
  mpz_roinit_n (d, ecc_b, size);

  mpz_init (u2);
  mpz_init (v2);
  mpz_init (w2);
  mpz_init (lhs);
  mpz_init (rhs);

  mpz_mul (u2, u, u);
  mpz_mul (v2, v, v);
  mpz_mul (w2, w, w);

  mpz_mul (rhs, u2, v2);
  mpz_mul (rhs, rhs, d);
  mpz_mul (lhs, w2, w2);
  mpz_add (rhs, rhs, lhs);

  mpz_add (lhs, u2, v2);
  mpz_mul (lhs, lhs, w2);

  res = mpz_congruent_p (lhs, rhs, mpz_roinit_n (t, ecc->p.m, size));

  mpz_clear (lhs);
  mpz_clear (rhs);
  mpz_clear (u2);
  mpz_clear (v2);
  mpz_clear (w2);

  gmp_free_limbs (tmp, (3 + 2) * size + ECC_A_TO_EH_GOST_ITCH(size));

  return res;
}

static void
ecc_gost_gc256a_a_to_eh (const struct ecc_curve *ecc,
			 mp_limb_t *r, const mp_limb_t *p,
			 mp_limb_t *scratch)
{
  ecc_a_to_eh_gost (ecc, ecc_s, ecc_t, r, p, scratch);
}

static void
ecc_gost_gc256a_eh_to_a (const struct ecc_curve *ecc,
			 int op,
			 mp_limb_t *r, const mp_limb_t *p,
			 mp_limb_t *scratch)
{
  ecc_eh_to_a_gost (ecc, ecc_s, ecc_t, op, r, p, scratch);

  if (op > 1)
    {
      unsigned shift;
      mp_limb_t cy;
      /* Reduce modulo q */
      shift = ecc->q.bit_size - 1 - GMP_NUMB_BITS * (ecc->p.size - 1);
      cy = mpn_submul_1 (r, ecc->q.m, ecc->p.size,
			 r[ecc->p.size-1] >> shift);
      assert (cy < 2);
      cnd_add_n (cy, r, ecc->q.m, ecc->p.size);
    }
}

const struct ecc_curve _nettle_gost_gc256a =
{
  {
    256,
    ECC_LIMB_SIZE,
    ECC_BMODP_SIZE,
    ECC_REDC_SIZE,
    ECC_MOD_INV_ITCH (ECC_LIMB_SIZE),
    0,

    ecc_p,
    ecc_Bmodp,
    ecc_Bmodp_shifted,
    ecc_redc_ppm1,

    ecc_pp1h,
    ecc_gost_gc256a_modp,
    ecc_gost_gc256a_modp,
    ecc_mod_inv,
    NULL,

    ecc_mod_mul_1_std,
    ecc_mod_submul_1_std,
  },
  {
    255,
    ECC_LIMB_SIZE,
    ECC_BMODQ_SIZE,
    0,
    ECC_MOD_INV_ITCH (ECC_LIMB_SIZE),
    0,

    ecc_q,
    ecc_Bmodq,
    ecc_mBmodq_shifted,
    NULL,
    ecc_qp1h,

    ecc_gost_gc256a_modq,
    ecc_gost_gc256a_modq,
    ecc_mod_inv,
    NULL,

    NULL,
    NULL,
  },

  USE_REDC,
  ECC_PIPPENGER_K,
  ECC_PIPPENGER_C,

  ECC_ADD_EH_ITCH (ECC_LIMB_SIZE),
  ECC_ADD_EHH_ITCH (ECC_LIMB_SIZE),
  ECC_DUP_EH_ITCH (ECC_LIMB_SIZE),
  ECC_MUL_A_EH_GOST_ITCH (ECC_LIMB_SIZE),
  ECC_MUL_G_EH_ITCH (ECC_LIMB_SIZE),
  ECC_A_TO_EH_GOST_ITCH (ECC_LIMB_SIZE),
  ECC_EH_TO_A_GOST_ITCH (ECC_LIMB_SIZE),

  ecc_add_eh,
  ecc_add_ehh,
  ecc_dup_eh,
  ecc_mul_a_eh_gost,
  ecc_mul_g_eh,
  ecc_gost_gc256a_a_to_eh,
  ecc_gost_gc256a_eh_to_a,
  ecc_gost_gc256a_is_valid,

  ecc_b, /* Edwards curve constant. */
  ecc_unit,
  ecc_table
};

const struct ecc_curve *nettle_get_gost_gc256a(void)
{
  return &_nettle_gost_gc256a;
}
