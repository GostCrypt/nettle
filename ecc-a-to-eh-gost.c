/* ecc-a-to-eh-gost.c

   Copyright (C) 2020 Dmity Baryshkov

   This file is part of GNU Nettle.

   GNU Nettle is free software: you can redistribute it and/or
   modify it under the terms of either:

     * the GNU Lesser General Public License as published by the Free
       Software Foundation; either version 3 of the License, or (at your
       option) any later version.

   or

     * the GNU General Public License as published by the Free
       Software Foundation; either version 2 of the License, or (at your
       option) any later version.

   or both in parallel, as here.

   GNU Nettle is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   General Public License for more details.

   You should have received copies of the GNU General Public License and
   the GNU Lesser General Public License along with this program.  If
   not, see http://www.gnu.org/licenses/.
*/

#if HAVE_CONFIG_H
# include "config.h"
#endif

#include "ecc.h"
#include "ecc-internal.h"

void
ecc_a_to_eh_gost (const struct ecc_curve *ecc,
		  const mp_limb_t *ecc_s, const mp_limb_t *ecc_t,
		  mp_limb_t *r, const mp_limb_t *p,
		  mp_limb_t *scratch)
{
  mp_size_t size = ecc->p.size;

  /*
     A = x - t
     B = A - s
     C = A + s

     U = A * C
     V = B * y
     W = A * C
     */
#define A scratch
#define B (scratch + size)
#define C (scratch + 2 * size)

#define X p
#define Y (p + size)

#define U r
#define V (r + size)
#define W (r + 2 * size)

  ecc_mod_sub(&ecc->p, A, X, ecc_t); /* x-t */
  ecc_mod_sub(&ecc->p, B, A, ecc_s); /* x-t-s */
  ecc_mod_add(&ecc->p, C, A, ecc_s); /* x-t+s */

  ecc_mod_mul (&ecc->p, U, A, C); /* U = (x-t) (x-t+s) */
  ecc_mod_mul (&ecc->p, V, Y, B); /* V = (x-t-s) y */
  /*
     If we allocate 4 sizes rather than 3 we can multiply directly to dest here
     A and B are not used anymore, so use them as temp space
   */
  ecc_mod_mul (&ecc->p, scratch, Y, C); /* W = y (x - t + s) */
  mpn_copyi(W, scratch, size);
#undef A
#undef B
#undef C
#undef X
#undef Y
#undef U
#undef V
#undef W
}

